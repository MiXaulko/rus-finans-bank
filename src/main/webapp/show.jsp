<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Table</title>
</head>
<body>
<div align="center">
    <table>
        <thead>
        <tr>
            <th>id</th>
            <th>first name</th>
        </tr>
        </thead>
        <c:forEach var="item" items="${values}">
            <tr>
                <td>
                        ${item.getId()}
                </td>
                <td>
                        ${item.getFirstName()}
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
