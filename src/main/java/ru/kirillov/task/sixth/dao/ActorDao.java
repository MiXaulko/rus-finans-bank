package ru.kirillov.task.sixth.dao;

import ru.kirillov.task.sixth.dto.ActorDto;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ActorDao {
    private final JdbcConnector connector = JdbcConnector.INSTANCE;

    public List<ActorDto> getAll() {
        List<ActorDto> list;
        try (Connection connection = connector.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("select * from actor limit 10")) {
            list = new ArrayList<>();
            while (resultSet.next()) {
                ActorDto dto = new ActorDto();
                dto.setId(resultSet.getInt(1));
                dto.setFirstName(resultSet.getString(2));
                list.add(dto);
            }
            return list;
        } catch (SQLException exception) {
            System.err.println("Произошла ошибка");
            exception.printStackTrace();
        }
        return Collections.emptyList();
    }
}
