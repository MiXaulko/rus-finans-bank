package ru.kirillov.task.sixth.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Locale;

public enum JdbcConnector {
    INSTANCE;
    private DataSource dataSource;

    JdbcConnector() {
        Context initContext = null;
        try {
            Locale.setDefault(Locale.ENGLISH);
            initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            dataSource = (DataSource) envContext.lookup("jdbc/mySql");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException ex) {
            System.err.println("Не удалось получить соединение");
            throw new RuntimeException(ex);
        }
    }
}
