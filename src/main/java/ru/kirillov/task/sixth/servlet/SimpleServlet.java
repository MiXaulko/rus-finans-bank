package ru.kirillov.task.sixth.servlet;

import ru.kirillov.task.sixth.dao.ActorDao;
import ru.kirillov.task.sixth.dto.ActorDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/")
public class SimpleServlet extends HttpServlet {
    private final ActorDao dao = new ActorDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ActorDto> res = dao.getAll();
        req.setAttribute("values", res);
        req.getRequestDispatcher("show.jsp").forward(req, resp);
    }
}
