package ru.kirillov.task.sixth.dto;

public class ActorDto {
    private int id;
    private String firstName;

    public ActorDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
