Изложите своё понимание MVP и MVC шаблонов проектирования пользовательского интерфейса. 
В чём заключается их отличие? По возможности проиллюстрируйте отличия поясняющими элементами кода.

![screenshot](https://i.imgur.com/BO3AT.jpg)

1. 	В MVP Model не общается с View

2. 	Presenter общается напрямую с View 
	Это позволяет, в случае необходимости, общаться Presenter-у с View's,  
	минуя Model приложения и событийный механизм с этим связанный, так как Presenter  
	в отличие от Controller, обладает необходимыми знаниями о View's,  
	а так же удаляет лишнюю логику обработки событий от Model из View.
	
3. 	Controller вызывает методы Model при реакции на событие, а Presenter управляет и Model's и View's