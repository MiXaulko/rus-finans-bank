package ru.kirillov.task.first;

public class Tree {
    private Node root;

    public void add(int value) {
        root = addRecursive(root, value);
    }

    private Node addRecursive(Node node, int value) {
        if (node == null) {
            return new Node(value);
        } else {
            if (value < node.value) {
                node.left = addRecursive(node.left, value);
            } else {
                if (value > node.value) {
                    node.right = addRecursive(node.right, value);
                } else {
                    return node;
                }
            }
            return node;
        }
    }

    public int sum() {
        return sumRecursive(root);
    }

    private int sumRecursive(Node node) {
        if (node != null) {
            return sumRecursive(node.left) + node.value + sumRecursive(node.right);
        } else {
            return 0;
        }
    }
}
