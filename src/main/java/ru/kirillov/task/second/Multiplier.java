package ru.kirillov.task.second;

import java.util.List;

public class Multiplier {
    public List<Integer> x2(List<Integer> list) {
        if (list == null) {
            return null;
        }
        for (int i = 0; i < list.size(); i++) {
            int variable = list.get(i);
            list.set(i, variable * variable);
        }

        return list;
    }
}
