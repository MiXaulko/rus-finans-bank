package ru.kirillov.task.third;

import java.util.Objects;

public class MathOperations<T extends Number> {
    public double add(T first, T second) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);
        return first.doubleValue() + second.doubleValue();
    }

    public double sub(T first, T second) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);
        return first.doubleValue() - second.doubleValue();
    }

    public double mul(T first, T second) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);
        return first.doubleValue() * second.doubleValue();
    }

    public double div(T first, T second) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);
        if (second.doubleValue() == 0) {
            throw new IllegalStateException("На 0 нельзя делить!");
        }
        return first.doubleValue() / second.doubleValue();
    }
}
