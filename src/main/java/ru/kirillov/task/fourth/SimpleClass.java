package ru.kirillov.task.fourth;

public class SimpleClass {
    private int value;

    public SimpleClass(){
    }

    public SimpleClass(int value) {
        this.value = value;
    }

    public void printValue() {
        System.out.println("Current value: "+ value);
    }
}
