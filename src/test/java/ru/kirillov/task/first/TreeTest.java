package ru.kirillov.task.first;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TreeTest {

    @Test
    void testTrueSum() {
        int[] values = {5, 32, 53, 7, 2, 4};
        Tree tree = new Tree();
        int sum = 0;
        for (int i = 0; i < values.length; i++) {
            int val = values[i];
            tree.add(val);
            sum += val;
        }
        assertEquals(sum, tree.sum());
    }

    @Test
    void testFalseSum() {
        Tree tree = new Tree();
        tree.add(1);
        assertNotEquals(42, tree.sum());
    }

    @Test
    void testEmptyTreeSum() {
        Tree tree = new Tree();
        assertEquals(0, tree.sum());
    }
}
