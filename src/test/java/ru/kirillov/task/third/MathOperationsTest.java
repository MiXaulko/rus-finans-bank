package ru.kirillov.task.third;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MathOperationsTest {
    MathOperations<Integer> integerMathOperations = new MathOperations<>();
    MathOperations<Double> doubleMathOperations = new MathOperations<>();
    MathOperations<Long> longMathOperations = new MathOperations<>();

    @Test
    void testCorrectAdd() {
        assertEquals(4, integerMathOperations.add(2, 2));
    }

    @Test
    void testIncorrectAdd() {
        assertNotEquals(5L, longMathOperations.add(2L, 2L));
    }

    @Test
    void testCorrectSub() {
        assertEquals(0, integerMathOperations.sub(2, 2));
    }

    @Test
    void testIncorrectSub() {
        assertNotEquals(5, integerMathOperations.sub(2, 2));
    }

    @Test
    void testDivByZero() {
        Assertions.assertThrows(IllegalStateException.class, () -> integerMathOperations.div(1, 0));
    }

    @Test
    void testCorrectDiv() {
        assertEquals(2, integerMathOperations.div(4, 2));
    }

    @Test
    void testIncorrectDiv() {
        assertNotEquals(0, integerMathOperations.div(4, 2));
    }

    @Test
    void testCorrectMul() {
        assertEquals(144.0, doubleMathOperations.mul(12.0, 12.0));
    }
}
