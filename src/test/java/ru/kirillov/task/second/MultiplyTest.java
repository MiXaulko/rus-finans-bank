package ru.kirillov.task.second;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplyTest {

    @Test
    void testTrueMultiply(){
        Multiplier multiplier = new Multiplier();
        int length = 10;
        List<Integer> list = new ArrayList<>(length);
        for(int i =0 ;i<length;i++){
            list.add(i);
        }
        int sum = list.stream().map(e-> e*e).reduce(Integer::sum).orElse(0);
        List<Integer> result = multiplier.x2(list);
        assertEquals(sum, result.stream().reduce(Integer::sum).orElse(0));
    }
}
