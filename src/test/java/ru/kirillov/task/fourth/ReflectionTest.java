package ru.kirillov.task.fourth;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReflectionTest {
    @Test
    void testConstructObject() {
        Class<SimpleClass> aClass = SimpleClass.class;
        Constructor<?>[] constructors = aClass.getDeclaredConstructors();
        for (Constructor c : constructors) {
            if (c.getParameterCount() == 0) {
                try {
                    SimpleClass instance = (SimpleClass) c.newInstance();
                    System.out.println(instance);
                    instance.printValue();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    System.out.println("Не получилось создать инстанс класс");
                }
            } else {
                Object[] params = new Object[c.getParameterCount()];
                Class<?>[] paramClasses = c.getParameterTypes();
                for (int i = 0; i < params.length; i++) {
                    Class pClass = paramClasses[i];
                    if (pClass.isPrimitive()) {
                        if (Integer.TYPE.isAssignableFrom(pClass)) {
                            params[i] = 10;
                        }
                    } else {
                        params[i] = new Object();
                    }
                }
                try {
                    SimpleClass instance = (SimpleClass) c.newInstance(params);
                    System.out.println(instance);
                    instance.printValue();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    System.out.println("Не получилось создать инстанс класс");
                }
            }
        }
    }

    @Test
    void testAccessToPrivateField() throws IllegalAccessException {
        int value = 141;
        int updatedValue = 999;
        SimpleClass simpleClass = new SimpleClass(value);
        Class<?> aClass = simpleClass.getClass();
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            assertEquals(value, field.get(simpleClass));
            if (Integer.TYPE.isAssignableFrom(field.getDeclaringClass())) {
                field.setInt(simpleClass, updatedValue);
                assertEquals(updatedValue,field.get(simpleClass));
            }
        }
    }

    @Test
    void testMethodStart() throws InvocationTargetException, IllegalAccessException {
        int value = 42;
        SimpleClass simpleClass = new SimpleClass(value);
        Class<?> aClass = simpleClass.getClass();
        Method[] methods = aClass.getDeclaredMethods();
        for(Method method: methods){
            if(method.getParameterCount()==0){
                method.invoke(simpleClass);
            }
        }
    }
}
